:docinfo: private
// :sectnums:
:xrefstyle: short
:icons: font
:doctype: book
:author: Mark Dönszelmann <Mark.Donszelmann@cern.ch>
:email: <Mark.Donszelmann@cern.ch>

ifeval::["{backend}" == "pdf"]
:toclevels: 4
:toc: left
endif::[]

// HTML
ifeval::["{backend}" == "html5"]
= ATLAS Operation Task Planner (OTP)

== User Manual

=== Mark Dönszelmann

include::version.adoc[]

https://otp-atlas.web.cern.ch

endif::[]

// PDF
ifeval::["{backend}" == "pdf"]
:imagesdir: ../images
= ATLAS Operation Task Planner (OTP): User Manual
WillBeReplacedByAuthor
include::version-pdf.adoc[]

toc::[]

:leveloffset: +1

include::1_introduction.adoc[]

include::2_shifts.adoc[]

include::3_work.adoc[]

include::4_tasks.adoc[]

include::5_booking_periods.adoc[]

include::6_reports.adoc[]

endif::[]
