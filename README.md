# Documentation How To

## Preview
- Run ./make-html.sh to try to create the single version site locally under build/site
- commit and push to 1.x branch will create the latest version on alpha (visible)
- alpha: https://otp-docs-atlas-alpha.docs.cern.ch

## Tagged
- ./make-tag.sh 0.5 will create a tagged version on alpha (visible after update in otp-docs)

## Preview multiple versions
- To check publication on alpha, check out master of otp-docs, and edit playbook file.
- Run ./make-html.sh to try to create the multi version site locally under build/site
- commit and push to master branch will create the multi version on alpha

## Production multiple versions
- To publish on prod, check out public of otp-docs, and edit the playbook file
- Run ./make-html.sh to try to create the multi version site locally under build/site
- commit and push to public branch will create the multi version on prod
- prod: https://otp-atlas.docs.cern.ch
