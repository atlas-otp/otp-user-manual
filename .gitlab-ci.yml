image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:el9-eos

variables:
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: normal
  GIT_DEPTH: 0
  PROJECT_NAME: otp-user-manual
  SSH_OPTIONS: -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPIDelegateCredentials=yes -o GSSAPITrustDNS=yes
  LOGIN: ${KRB_USERNAME}@lxplus.cern.ch
  EOSPROJECT: root://eosproject-o.cern.ch/
  SITE: build/site
  PDFS: /eos/project/o/otp/www/pdfs/
  MANUAL: ${SITE}/${PROJECT_NAME}/${CI_COMMIT_REF_NAME}
  ATTACHMENTS: ${SITE}/${PROJECT_NAME}/${CI_COMMIT_REF_NAME}/_attachments

stages:
- build
- pdf-build
- deploy
- trigger

build:
  image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:antora3-lunr
  stage: build
  script:
  - npm list -g --depth 0
  - antora --version
  - rm -fr ${SITE}
  - git clone --branch master --single-branch https://oauth2:$ACCESS_TOKEN@gitlab.cern.ch//atlas-otp/otp-manuals-deploy.git ${SITE}
  - mkdir -p ${SITE}
  - npm install @antora/lunr-extension @antora/site-generator tar-stream fs-extra git-describe
  - git rev-parse --abbrev-ref HEAD
  - git branch --remote --verbose --no-abbrev --contains  | sed -rne 's/^[^\/]*\/([^\ ]+).*$/\1/p'
  - ./make-html.sh ${CI_COMMIT_REF_NAME}
  # Needs to be run here as the image for pdf does not have antora
  - antora --extension=Cc_export antora-playbook.yml --stacktrace --log-level debug --log-failure-level error
  artifacts:
    expire_in: 1 day
    paths:
    - ${SITE}
    - Cc
    - modules/ROOT/pages/version-pdf.adoc

pdf-build:
  image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:ruby-asciidoctor
  stage: pdf-build
  needs: ["build"]
  script:
  - mkdir -p ${ATTACHMENTS}
  # No antora so run seperate items
  # ./make-pdf.sh ${CI_COMMIT_REF_NAME}
  - mkdir -p build/site/${PROJECT_NAME}/${CI_COMMIT_REF_NAME}/_attachments
  - asciidoctor-pdf --failure-level ERROR --warnings Cc/modules/ROOT/pages/${PROJECT_NAME}.adoc  -o build/site/${PROJECT_NAME}/${CI_COMMIT_REF_NAME}/_attachments/${PROJECT_NAME}.pdf
  - find build
  artifacts:
    expire_in: 1 day
    paths:
    - ${SITE}

deploy-version:
  stage: deploy
  needs: ["build"]
  script:
  - echo "adding version file to ${CI_COMMIT_REF_NAME}"
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - cat ${MANUAL}/version.adoc
  - EOS_MGM_URL=${EOSPROJECT} eos cp -r -n ${MANUAL}/version.adoc ${PDFS}/${PROJECT_NAME}-${CI_COMMIT_REF_NAME}-version.adoc

deploy-pdf:
  stage: deploy
  environment: staging
  needs: ["pdf-build"]
  script:
  - echo "adding pdf to ${CI_COMMIT_REF_NAME}"
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - EOS_MGM_URL=${EOSPROJECT} eos cp -r -n ${ATTACHMENTS}/${PROJECT_NAME}.pdf ${PDFS}/${PROJECT_NAME}-${CI_COMMIT_REF_NAME}.pdf

copy:
  image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:cc7-py3
  stage: deploy
  needs: ["build", "pdf-build"]
  script:
  # Deploy
  - export MSG=$(git log -1 --pretty=%B)
  - cd ${SITE}
  - git status
  - git diff
  - git add --all
  - git status
  - git config --global user.email "Mark.Donszelmann@cern.ch"
  - git config --global user.name "Auto-Update"
  - git config --global push.default simple
  - git config --global pull.rebase false
  - echo ${CI_COMMIT_SHORT_SHA}
  - echo ${CI_COMMIT_TAG}
  - git diff-index --quiet HEAD || git commit -m "sha ${CI_COMMIT_SHORT_SHA} - tag ${CI_COMMIT_tag} - pipeline ${CI_PIPELINE_ID} - msg '${MSG}'"
  - git log -2
  - git remote add krb https://oauth2:$ACCESS_TOKEN@gitlab.cern.ch//atlas-otp/otp-manuals-deploy.git
  - git pull -X ours krb master
  - git push krb master
  only:
  - tags
  - branches
  artifacts:
    expire_in: 1 day
    paths:
    - ${SITE}

tag:
  image: gitlab-registry.cern.ch/atlas-otp/otp-gitlab-ci-runner:cc7-py3
  stage: deploy
  needs: ["copy"]
  script:
  # Deploy
  - cd ${SITE}
  - git status
  - git config --global user.email "Mark.Donszelmann@cern.ch"
  - git config --global user.name "Auto-Update"
  - git config --global push.default simple
  - git config --global pull.rebase false
  - echo ${CI_COMMIT_TAG}
  - git tag ${CI_COMMIT_TAG}
  - git remote add krb-tag https://oauth2:$ACCESS_TOKEN@gitlab.cern.ch//atlas-otp/otp-manuals-deploy.git
  - git push krb-tag --tags
  only:
  - tags

otp-docs-alpha:
  stage: trigger
  needs: ["deploy-version", "deploy-pdf"]
  trigger:
    project: atlas-otp/otp-docs
    branch: master
    strategy: depend
    forward:
      yaml_variables: false
  allow_failure: true
